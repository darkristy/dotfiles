# Aliases
alias git=hub
alias tmux='tmux -2'
alias update="source ~/.zshrc"
# ---------
alias config="/usr/bin/git --git-dir=$HOME/Documents/dotfiles --work-tree=$HOME"
alias zshconfig="code ~/.zshrc"
alias vimconfig="code ~/.config/nvim/init.vim"
alias aconfig="code ~/.aliases.bash"
# ---------
alias ls="colorls"
alias la="colorls -a"
alias nv="nvim"
# ---------
alias projects="cd /media/darkristy/Samsung_T5/projects"
alias born="cd /media/darkristy/Samsung_T5/projects/bornwhitfield.space"
# ---------
alias pip="pip3"
alias open="code ."
# ---------
alias ga="git add ."
alias gam="git commit -a -m "
alias gb="git branch"
alias gs="git status"
alias gba="git branch $1"
alias gbd="git branch -D $1"
alias gcm="git checkout master"
alias gcd="git checkout develop"
alias gcr="git checkout release"
alias gcb="git checkout $1"
alias gma="git merge abort"
alias gmm="git merge master"
alias gpo="git push origin $1"
alias gpom="git push origin master"
alias gpro="git pull --rebase origin master"
alias gpru="git pull --rebase upstream master"
alias gra="git rebase --abort"
alias grc="git rebase --continue"
alias gpull="git pull"
